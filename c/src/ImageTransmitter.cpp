/*
 * ImageTransmitter.cpp
 *
 *  Created on: Nov 4, 2019
 *      Author: se3910
 */

#include "ImageTransmitter.h"
#include <stdlib.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <chrono>
#include <unistd.h>
#include "time_util.h"


using namespace cv;
using namespace std;

ImageTransmitter::ImageTransmitter(char *machineName, int port, int linesPerUDPDatagram) {
	this->destinationMachineName = machineName;
	this->myPort = port;
	this->linesPerUDPDatagram = linesPerUDPDatagram;
}

ImageTransmitter::~ImageTransmitter() {
	delete this->destinationMachineName;
}

int ImageTransmitter::streamImage(Mat *image) {
	if (image != NULL && this->destinationMachineName != NULL) {
//		std::cout << "streaming image." << std::endl;
		// start setting up socket
		struct sockaddr_in serv_addr;
		struct hostent *server;
		int serverlen;

		// create socket and check that it is valid
		int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
		if(sockfd < 0) {
			std::cerr << "ERROR: Opening socket failed." << std::endl;
			exit(-1);
		}

		// get host to send to and check that it is valid
		server = gethostbyname(this->destinationMachineName);
		if(server == NULL) {
			std::cerr << "ERROR: No such hostname." << std::endl;
			exit(-1);
		}

		// zero out structure, set structure's type of address, and copy over appropriate data
		bzero((char *) &serv_addr, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		memcpy((char *) &serv_addr.sin_addr.s_addr, (char *) server->h_addr, server->h_length);

		// convert byte order to network order
		serv_addr.sin_port = htons(this->myPort);
		serverlen = sizeof(serv_addr);

		// prepare message for sending
		int rowLength = 3 * image->cols + 24;
		int totalMessageSize = this->linesPerUDPDatagram * rowLength;
		int32_t  *msg = (int32_t*) malloc(totalMessageSize);
		uint32_t time = current_timestamp();

		//setup outerloop for creating a message
		for(int index = 0; index < image->rows / this->linesPerUDPDatagram; index++) {
			int row = index * this->linesPerUDPDatagram;

			// setup inner loop that will send datagrams with the possibility of multiple rows per datagram
			int rowInPacket = 0;
			while(row < image->rows && rowInPacket < this->linesPerUDPDatagram) {
				msg[0 + rowInPacket * rowLength/4] = htonl(time);
				msg[1 + rowInPacket * rowLength/4] = htonl(current_timestamp());
				msg[2 + rowInPacket * rowLength/4] = htonl(this->imageCount);
				msg[3 + rowInPacket * rowLength/4] = htonl(image->rows);
				msg[4 + rowInPacket * rowLength/4] = htonl(image->cols);
				msg[5 + rowInPacket * rowLength/4] = htonl(row);

//				cout << "Start Time: " << ntohl(temp[0 + rowInPacket * rowLength]) << endl;
//                cout << "Current Time: " << ntohl(temp[1 + rowInPacket * rowLength]) << endl;
//                cout << "Image Count: " << ntohl(temp[2 + rowInPacket * rowLength]) << endl;
//                cout << "Number Rows: " << ntohl(temp[3 + rowInPacket * rowLength]) << endl;
//                cout << "Number Cols: " << ntohl(temp[4 + rowInPacket * rowLength]) << endl;
//                cout << "Index: " << ntohl(temp[5 + rowInPacket * rowLength]) << endl;

				memcpy(&msg[6 + rowInPacket * rowLength/4], &image->data[image->cols * row * 3], image->cols * 3);
				row++;
				rowInPacket++;
			}
//			printf("sending packet %i row %i\n", index, row-1);


			int lres = sendto(sockfd, msg, totalMessageSize, 0, (struct sockaddr *) &serv_addr, serverlen);

			if(lres < 0) {
				std::cerr << "Error: " << errno << std::endl;
				exit(-1);
			} else {
//				std::cout << "message sent" << std::endl;
			}
		}

		// send image
		this->imageCount++;
		// delete image;
		close(sockfd);
		return 0;
	} else {
		printf("Failed to stream image");
		return -1;
	}
	return 0;
}



