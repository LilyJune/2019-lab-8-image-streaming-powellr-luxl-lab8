import java.io.IOException;
import javax.swing.WindowConstants;
import edu.msoe.sefocus.pcgui.STreamedImageFrame;
import edu.msoe.sefocus.pcgui.StreamedImagePanel;

public class Main {
	public static void main(String args[]) throws IOException, InterruptedException {
		if (args.length!=4)
		{
			System.err.println("Usage: <Program Name> <Port> <Width> <Height> <Lines Per UDP Message>");
		}
		else {
			int port = Integer.parseInt(args[0]);
			int width = Integer.parseInt(args[1]);
			int height = Integer.parseInt(args[2]);
			int linesPerUDPTransmission = Integer.parseInt(args[3]);

			STreamedImageFrame frame = new STreamedImageFrame(port, width, height, linesPerUDPTransmission, System.out);

			frame.setVisible(true);
			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

			frame.startReceivingImages();
			frame.waitToStopReceivingImages();
		}

	}
}
